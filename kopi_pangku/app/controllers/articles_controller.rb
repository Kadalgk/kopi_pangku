class ArticlesController < ApplicationController
	skip_before_action :authenticate_request, only: [:show, :update, :delete]
	before_action :set_article, only: [:show, :update, :delete]

	def create
		@post = current_user.articles.create(post_params)
		if @post.save
			render json: { result: true, post: @post }, status: :created
	  	else
	   		render json: { result: false, post: @post.errors }, status: :unprocessable_entity
	  	end
	end

	def show
  		render json: { result: true, post: @article }
	end

	def show_by_userid
  		@article = current_user.articles
  		render json: { posts: @article }
 	end


	private 
	def post_params
		params.permit(
			:tittle,
			:content
			)
	end

	def set_article
		@article = Article.find(params[:id])
	end

	
end