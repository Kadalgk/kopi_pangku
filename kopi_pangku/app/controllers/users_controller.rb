class UsersController < ApplicationController
	skip_before_action :authenticate_request, only: %i[login register]
	before_action :set_user, only: [:show]	
 # POST /register
  def register
    @user = User.create(user_params)
   if @user.save
    response = { message: 'User created successfully'}
    render json: response, status: :created 
   else
    render json: @user.errors, status: :bad
   end 
  end

  def login
    authenticate params[:email], params[:password]
  end
  def test
    render json: {
          message: 'You have passed authentication and authorization test'
        }
  end
  def show
  	render json: { result: true, user: @user } 
  end
  def index
  @users = User.all
  render json: { result: true, users: @users }, status: :ok
 end



  private

  def user_params
    params.permit(
      :name,
      :email,
      :password
    )
  end

  def authenticate(email, password)
    command = AuthenticateUser.call(email, password)

    if command.success?
      render json: {
        access_token: command.result,
        message: 'Login Successful'
      }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
   end
   def set_user
   	@user = current_user
   end
end
