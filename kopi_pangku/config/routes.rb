Rails.application.routes.draw do
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #add our register route
   post 'auth/register', to: 'user#register'
   post 'auth/login', to: 'users#login'
   get 'test', to: 'users#test'
   get 'show', to: 'users#show'
   get 'users', to: 'users#index'


   post 'createpost', to: 'articles#create'
   get 'showarticle/:id', to: 'articles#show'
   get 'show_by_id', to: 'articles#show_by_userid'
end
